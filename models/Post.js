const mongoose = require('mongoose');

const { Schema } = mongoose;
const postSchema = new Schema({
  text: {
    type: String,
    required: true,
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User',
  },
  date: {
    type: Date,
    default: Date.now,
  },
});
const Post = mongoose.model('posts', postSchema);
module.exports = Post;
