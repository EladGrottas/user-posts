/* eslint-disable no-use-before-define */
import axios from 'axios';
import { GET_ERRORS, SET_USER_POSTS } from './types';

export const createNewPost = (postData, history) => (dispatch) => {
  axios
    .post('http://localhost:5000/api/posts', postData)
    .then(() => history.push('/posts')) // re-direct to login on successful register
    .catch((err) => dispatch({
      type: GET_ERRORS,
      payload: err.response.data,
    }));
};

export const getUserPosts = (user) => (dispatch) => {
  axios
    .get('http://localhost:5000/api/posts', {
      params: {
        user,
      },
    })
    .then((res) => {
      dispatch(setUserPosts(res.data));
    })
    .catch((err) => dispatch({
      type: GET_ERRORS,
      payload: err.response.data,
    }));
};

export const setUserPosts = (posts) => ({
  type: SET_USER_POSTS,
  payload: posts,
});
