/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { useState, useEffect } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { createNewPost } from '../../actions/postActions';

const Register = ({ history, createNewPost, auth }) => {
  const [text, setText] = useState('');
  useEffect(() => {
    if (!auth.isAuthenticated) {
      history.push('/');
    }
  });
  const onChange = (e) => {
    setText(e.target.value);
  };
  const onSubmit = (e) => {
    e.preventDefault();
    const newPost = {
      text,
      user: auth.user.id,
    };
    createNewPost(newPost, history);
  };
  return (
    <div className="container">
      <div className="row">
        <div className="col s8 offset-s2">
          <Link to="/posts" className="btn-flat waves-effect">
            <i className="material-icons left">keyboard_backspace</i>
            {' '}
            Back to
            posts
          </Link>
          <form noValidate onSubmit={onSubmit}>
            <div className="input-field col s12">
              <textarea
                className="materialize-textarea"
                onChange={onChange}
                value={text}
                id="name"
                type="text"
              />
              <label htmlFor="textarea1">Post</label>
            </div>
            <div className="col s12" style={{ paddingLeft: '11.250px' }}>
              <button
                style={{
                  width: '150px',
                  borderRadius: '3px',
                  letterSpacing: '1.5px',
                  marginTop: '1rem',
                }}
                type="submit"
                className="btn btn-large waves-effect waves-light hoverable blue accent-3"
              >
                Post
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(
  mapStateToProps,
  { createNewPost },
)(withRouter(Register));
