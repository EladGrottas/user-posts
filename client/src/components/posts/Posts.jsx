/* eslint-disable no-underscore-dangle */
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { getUserPosts } from '../../actions/postActions';
import Post from './Post';

const Posts = ({
  getUserPosts, posts, auth, history,
}) => {
  useEffect(() => {
    if (!auth.isAuthenticated) {
      history.push('/');
    }
  });
  useEffect(() => {
    getUserPosts(auth.user.id);
  }, []);

  const { user } = auth;
  return (
    <div className="container valign-wrapper">
      <div className="row">
        <div className="col s12 center">
          <h1>
            Welcome
            {' '}
            {user.name}
          </h1>
          {
            posts.length ? posts.map((post) => (
              <Post key={post._id} post={post} />
            ))
              : null
          }
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  auth: state.auth,
  posts: state.posts,
});
export default connect(
  mapStateToProps,
  { getUserPosts },
)(Posts);
