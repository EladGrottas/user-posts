import React from 'react';

const Post = ({ post }) => (
  <div className="row">
    <div className="col s12 center">
      <div className="card-panel">
        <span className="black-text">
          {post.text}
        </span>
      </div>
    </div>
  </div>
);

export default Post;
