import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { logoutUser } from '../../actions/authActions';

const Navbar = ({ auth, logoutUser }) => {
  const onLogoutClick = (e) => {
    e.preventDefault();
    logoutUser();
  };
  const { user } = auth;
  return (
    <div className="navbar-fixed">
      <nav className="z-depth-0">
        <div className="nav-wrapper white">
          {user.name
            ? (
              <Link
                to="/posts"
                style={{ fontFamily: 'monospace' }}
                className="brand-logo black-text"
              >
                <i className="material-icons">code</i>
                Posts
              </Link>
            )
            : (
              <Link
                to="/"
                style={{ fontFamily: 'monospace' }}
                className="brand-logo black-text"
              >
                <i className="material-icons">code</i>
                Posts
              </Link>
            )}
          {user.name
            ? (
              <ul className="right hide-on-med-and-down">
                <li>
                  <Link
                    to="/create-post"
                    style={{
                      borderRadius: '3px',
                      letterSpacing: '1.5px',
                    }}
                    className="btn btn-large btn-flat waves-effect white black-text"
                  >
                    New Post
                  </Link>
                </li>
                <li>
                  <Link
                    to="/"
                    style={{
                      width: '140px',
                      borderRadius: '3px',
                      letterSpacing: '1.5px',
                    }}
                    onClick={onLogoutClick}
                    className="btn btn-large waves-effect waves-light hoverable blue accent-3"
                  >
                    Logout
                  </Link>
                </li>
              </ul>
            )
            : (
              <ul className="right hide-on-med-and-down">
                <li>
                  <Link
                    to="/login"
                    style={{
                      width: '140px',
                      borderRadius: '3px',
                      letterSpacing: '1.5px',
                    }}
                    className="btn btn-large btn-flat waves-effect white black-text"
                  >
                    Log In
                  </Link>
                </li>
                <li>
                  <Link
                    to="/register"
                    style={{
                      width: '140px',
                      borderRadius: '3px',
                      letterSpacing: '1.5px',
                    }}
                    className="btn btn-large waves-effect waves-light hoverable blue accent-3"
                  >
                    Register
                  </Link>
                </li>
              </ul>
            )}
        </div>
      </nav>
    </div>
  );
};

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(
  mapStateToProps,
  { logoutUser },
)(Navbar);
