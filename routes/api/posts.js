/* eslint-disable no-console */
const express = require('express');

const router = express.Router();

const Post = require('../../models/Post');

router.post('/', (req, res) => {
  const newPost = new Post({
    text: req.body.text,
    user: req.body.user,
  });
  newPost
    .save()
    .then((user) => res.json(user))
    .catch((err) => console.log(err));
});

router.get('/', (req, res) => {
  Post.find({ user: req.query.user }).then((posts) => {
    res.json(posts);
  });
});

module.exports = router;
