
# Run Project

## Run Server

`npm install`
`npm run server`

## Run Client

`cd client`
`npm install`
`npm start`

## Notes

1. I didn't get to the part of testing with jest because of lack of time.
2. I use config/keys and not .env because I felt more comfortable using this.
